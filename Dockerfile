#stage 1
FROM node:20.5.1 as node
WORKDIR /app
COPY . .
RUN npm install --force
RUN npm run build --prod

#stage 2
FROM nginx:alpine
COPY --from=node /app/dist/webo_application /usr/share/nginx/html
